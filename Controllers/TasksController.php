<?php

namespace MVC\Controllers;

use MVC\Core\Controller;
use MVC\Models\Task;
use MVC\Models\TaskRepository;

class TasksController extends Controller
{
    public function index()
    {
        $taskRepo = new TaskRepository();
        $d['tasks'] = $taskRepo->getAll();
        $this->set($d);
        $this->render("index");
    }

    public function create()
    {
        if (isset($_POST["title"]))
        {
            $task = new Task();
            $taskRepo = new TaskRepository();
            $task->title = $_POST['title'];
            $task->description = $_POST['description'];
            $task->created_at = date('Y-m-d H:i:s');
            $task->updated_at = date('Y-m-d H:i:s');
            if ($taskRepo->add($task))
            {
                header("Location: " . WEBROOT . "tasks/index");
            }
        }

        $this->render("create");
    }

    public function edit($id)
    {
        $task = new Task();
        $taskRepo = new TaskRepository();
        $d["task"] = $taskRepo->get($id);
        if (isset($_POST["title"]))
        {
            $task->id = $id;
            $task->title = $_POST['title'];
            $task->description = $_POST['description'];
            $task->updated_at = date('Y-m-d H:i:s');
            if ($taskRepo->update($task))
            {
                header("Location: " . WEBROOT . "tasks/index");
            }
        }
        $this->set($d);
        $this->render("edit");
    }

    public function delete($id)
    {
        $task = new Task();
        $taskRepo = new TaskRepository();
        if ($taskRepo->delete($id))
        {
            header("Location: " . WEBROOT . "tasks/index");
        }
    }
}
?>