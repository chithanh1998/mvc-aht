<?php

namespace MVC\Models;

use MVC\Core\ResourceModel;
use MVC\Models\Task;

class TaskResource extends ResourceModel
{
    public function __construct()
    {
        $task = new Task();
        parent::_init( 'tasks', null, $task);
    }
}

?>