<?php

namespace MVC\Models;

use MVC\Models\TaskResource;

class TaskRepository 
{
    public function add($model)
    {
        $task = new TaskResource();
        return $task->save($model);
    }
    public function get($id)
    {
        $task =  new TaskResource();
        return $task->find($id);
    }
    public function getAll()
    {
        $task = new TaskResource();
        return $task->getAll();
    }
    public function update($model){
        $task = new TaskResource();
        return $task->save($model);
    }
    public function delete($id)
    {
        $task = new TaskResource();
        return $task->delete($id);
    }
    
}

?>