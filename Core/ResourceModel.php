<?php

namespace MVC\Core;

use MVC\Core\ResourceModelInterface;
use MVC\Config\Database;
use MVC\Core\Model;
use PDO;

class ResourceModel implements ResourceModelInterface
{
    private $id;
    private $model;
    private $table;

    public function _init($table, $id, $model)
    {
        $this->id = $id;
        $this->model = $model;
        $this->table = $table;
    }
    public function find($id)
    {
        $query = "SELECT * FROM $this->table WHERE id = " . $id;
        $sql = Database::getBdd()->prepare($query);
        $sql->execute();
        return $sql->fetch(PDO::FETCH_OBJ);
    }
    public function getAll()
    {
        $query = "SELECT * FROM {$this->table}";
        $sql = Database::getBdd()->prepare($query);
        $sql->execute();
        return $sql->fetchAll(PDO::FETCH_OBJ);
    }
    public function save($model)
    {
        $properties = $model->getProperties();
        $placeholder = [];
        $placeUpdate = [];
        $insert_key = [];
        if($model->id === null){
            // Insert
            unset($properties["id"]);
            foreach($properties as $k => $v){
                $insert_key[] = $k;
                $placeholder[] = ":".$k;
            }
            $strPlaceholder = implode(", ", $placeholder);
            $strInsKey = implode(", ", $insert_key);
            $query = "INSERT INTO " . $this->table . " (" . $strInsKey . ") VALUES (" . $strPlaceholder . ")";
            $sql = Database::getBdd()->prepare($query);
            return $sql->execute($properties);
        } else {
            // Update
            unset($properties["created_at"]);
            foreach($properties as $k => $v){
                $placeUpdate[] = $k . " = :" . $k;
            }
            $strPlaceUpdate = implode(", ", $placeUpdate);
            $query = "UPDATE " . $this->table . " SET " . $strPlaceUpdate . " WHERE id = :id";
            $sql = Database::getBdd()->prepare($query);
            return $sql->execute($properties);
        }

    }
    public function delete($id)
    {
        $query = "DELETE FROM " . $this->table . " WHERE id = " . $id;
        $sql = Database::getBdd()->prepare($query);
        return $sql->execute();
    }
}

?>